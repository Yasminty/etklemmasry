import { Injectable } from '@angular/core';
import { Router } from  "@angular/router";
import { auth } from  'firebase/app';
import { AngularFireAuth } from  "@angular/fire/auth";
import { User } from  'firebase';
import { stringify } from 'querystring';
import { first } from 'rxjs/operators';
import * as firebase from "firebase";



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public  afAuth:  AngularFireAuth, public  router:  Router) {
    this.afAuth.authState.subscribe(user => {
      if (user){
        this.user = user;
        localStorage.setItem('user', stringify(this.user));
      } else {
        localStorage.setItem('user', null);
      }
    })
   }
  user: User;

  async  login(email:  string, password:  string) {
    
    try {
        await  this.afAuth.auth.signInWithEmailAndPassword(email, password)
        this.router.navigate(['test']);
    } catch (e) {
        alert("Error!"  +  e.message);
    }
    }

  async  loginWithGoogle(){
    await  this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
    this.router.navigate(['test']);
}
async  loginWithFB(){
  await  this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider())
  this.router.navigate(['test']);
  
}


  
}
