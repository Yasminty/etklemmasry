import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from 'src/app/index/index.component';
import { HomeComponent } from 'src/app/home/home.component';
import { LoginComponent } from 'src/app/login/login.component';
import { TestComponent } from 'src/app/test/test.component';

const routes: Routes = [
  {path:'',component:IndexComponent},
  {path:'home',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'test',component:TestComponent},
  

  
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
