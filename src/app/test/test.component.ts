import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor( public  router:  Router) { }

  ngOnInit() {
  }
  formSubmit(){
    this.router.navigate(['home']);
   
  }
  QN: any[]=[
    {
      'q':"Question1",
      'as':[
        {a:"Answer1"}, {a:"Answer2"}, {a:"Answer3"}
       
      ]
    },
    {
      'q':"Question2",
      'as':[
        {a:"Answer1"}, {a:"Answer2"}, {a:"Answer3"}
       
      ]
    },
    {
      'q':"Question3",
      'as':[
        {a:"Answer1"}, {a:"Answer2"}, {a:"Answer3"}
       
      ]
    },
    {
      'q':"Question4",
      'as':[
        {a:"Answer1"}, {a:"Answer2"}, {a:"Answer3"}
       
      ]
    },
    {
      'q':"Question5",
      'as':[
        {a:"Answer1"}, {a:"Answer2"}, {a:"Answer3"}
       
      ]
    }
    ];


}
