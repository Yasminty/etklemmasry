import { Component, OnInit, LOCALE_ID, Inject } from '@angular/core';
import { AuthService } from  '../auth/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from "firebase";
import { Router } from '@angular/router';



@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  user:firebase.User;
  languageList = [
    { code: 'en', label: 'English' },
    { code: 'ar', label: 'Arabic' }

  ];

  constructor(@Inject(LOCALE_ID) protected localeId: string,private  afAuth:  AngularFireAuth,public  router:  Router) {
    afAuth.authState.subscribe(user => this.user=user);
   }


  ngOnInit() {
  }

}
