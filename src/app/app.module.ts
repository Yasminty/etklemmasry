import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID,NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { IndexComponent } from './index/index.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { TestComponent } from './test/test.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from "@angular/fire";
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from 'src/environments/environment.prod';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


var config = {
  apiKey: "AIzaSyBcuNlwjLSKtLPxi15W3Te9gqNXwURc6TI",
  authDomain: "etklemmasry.firebaseapp.com",
  databaseURL: "https://etklemmasry.firebaseio.com",
  projectId: "etklemmasry",
  storageBucket: "etklemmasry.appspot.com",
  messagingSenderId: "256716919525",
  appId: "1:256716919525:web:0479da4e2f09b437"
};
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    IndexComponent,
    HomeComponent,
    LoginComponent,
    TestComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    AngularFontAwesomeModule,
    FormsModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
