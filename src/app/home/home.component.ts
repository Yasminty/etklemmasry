import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }
  level: number=4;
  step: number=4;
  points: number=100;
  
  

  ngOnInit() {
  }

}
